// sticky search bar mobile
if($('.searchBar').offset()){
    if(window.screen.width<=1023){
        $(document).ready(function(){
        var stickyschlrshipBtn=$('main').offset().top-(-30);
        $(window).scroll(function(){
        var sticky=$('.searchBar');
        var scroll=$(window).scrollTop();
        if(scroll>=stickyschlrshipBtn){
            sticky.addClass('fixedSearchBar');}
        else{
            sticky.removeClass('fixedSearchBar');
        }
    });
    });
}}
 $(".selfLearningType ul li").click(function () {
    var tab_id = $(this).attr("data-tab");
    $(".selfLearningType ul li").removeClass("current");
    $(".tab-content").removeClass("current");
    $(this).addClass("current");
    $("#" + tab_id).addClass("current");
});



$(".selfLearningSlider, .reviewList, .fetureSlider").scroll(function () {
    var $width = $(this).outerWidth();
    var $scrollWidth = $(this)[0].scrollWidth;
    var $scrollLeft = $(this).scrollLeft();
    if (parseInt($scrollWidth - $width) === parseInt($scrollLeft)) {
        $(this).prev().addClass("over");
    } else {
        $(this).prev().removeClass("over");
    }
    if ($scrollLeft === 0) {
        $(this).prev().prev().addClass("over");
    } else {
        $(this).prev().prev().removeClass("over");
    }
});
$('.selfLearningList .scrollLeft').click(function () {
    $(this).next().next().animate({
        scrollLeft: '-=395px'
    }, 750);

});
$('.selfLearningList .scrollRight').click(function () {
    $(this).next().animate({
        scrollLeft: '+=395px'
    }, 750);

    $(this).prev().removeClass("over");
});


$('.reviewWrap .scrollLeft').click(function () {
    $(this).next().next().animate({
        scrollLeft: '-=395px'
    }, 750);

});
$('.reviewWrap .scrollRight').click(function () {
    $(this).next().animate({
        scrollLeft: '+=395px'
    }, 750);

    $(this).prev().removeClass("over");
});
$('.feturesList .scrollLeft').click(function () {
    $(this).next().next().animate({
        scrollLeft: '-=395px'
    }, 750);

});
$('.feturesList .scrollRight').click(function () {
    $(this).next().animate({
        scrollLeft: '+=395px'
    }, 750);

    $(this).prev().removeClass("over");
});


$(".faqQuestion").click(function () {
    $(this).toggleClass("downAngle");
    $(this).next().slideToggle("slow")
    .siblings(".faqAnswer:visible").slideUp("slow");
});



var brandCount = $(".featuredBrand").length;

$('.featuredBrandList').slick({
    speed: 4000,
    autoplay: true,
    autoplaySpeed: 1000,
    centerMode: true,
    cssEase: 'linear',
    slidesToShow: 1,
    slidesToScroll: 1,
    variableWidth: true,
    infinite: true,
    initialSlide: 1,
    arrows: false,
    buttons: false
});


// footer dropdown
if (window.screen.width <= 767) {
    $(".footrer_main_content ul").addClass("active");
    $(".footrer_main_content h4").click(function () {
        $(this).next(".footrer_main_content ul").slideToggle("slow").removeClass("active");
        $(this).toggleClass("angle");
    });
}




 // header mobile slider

 if (window.screen.width < 1023) {
    $(".navDropdownMenu a").click(function () {
        $(this).next().css({ "visibility": "visible", "width": "100%", "padding": "16px" });
    });
    $(".subMenuHeading").click(function () {
        $(this).next().css({ "visibility": "visible", "width": "100%", "padding": "16px" });
    });
    $(".goBackToMenu").click(function () {
        $(this).parent().css({ "visibility": "hidden", "width": "0%", "padding": "0px" });
    });

    $(".hambergerMenuIcon").click(function () {
        $(".greyBgMobile").css("width", "100%");
        $(".megaMenu").css("width", "80%");
    });   

    $(".greyBgMobile").on("click", function (event) {
        var $trigger = $(".greyBgMobile");
        if ($trigger !== event.target && !$trigger.has(event.target).length) {
            $(".megaMenu, .greyBgMobile").css("width", "0%");
        }
    });
}


if($(".startHere").length >0 ){
	$(".startHere").on('click', function() { 
		$("#leadForm").addClass("open_it");
	});
}
 
$(".closeIcon,.shadowpt,.blackCloseIcon,.backArrowIcon").on('click', function() { 
    $("#leadForm").removeClass("open_it");
});

$(document).on('click','.eyeIcon',function(){
    var eye=$('.eyeIcon');
    eye.toggleClass("eye-slash");
    if(eye.hasClass('eye-slash')){
        $('.inputPassword').attr('type','password');
    }
    else{
        $('.inputPassword').attr('type','text');
    }
});


$('.locationTabbing li').click(function () {
    var tab_id = $(this).attr('data-tab');
    $('.locationTabbing li').removeClass('active');
    $(".loactionWrap .tab-content").removeClass('active');
    $(this).addClass('active');
    $("#" + tab_id).addClass('active');
});
$(".loactionAccordian").click(function () {
    $(this).toggleClass("downAngle");
    $(this).next().slideToggle("slow")
        .siblings(".locationAccordianContent:visible").slideUp("slow");
});


$(document).ready(function() {
    $('.serviceList ul').find('li:eq(0)').addClass('active');
    $('.serviceWrapcontent .tab-data').find('> div:eq(0)').nextAll().hide();
    $('.serviceList ul li').click( function(event) {
            event.preventDefault();
        $('.serviceList ul li').removeClass();
        $('.serviceWrapcontent .tab-data > div').hide();
            $(this).addClass('active');
            var index = $('.serviceList ul li').index(this);
        $('.serviceWrapcontent .tab-data > div:eq('+index+')').show();

    });

});

if (window.screen.width <= 767) {
    $(".serviceContent").addClass("active");
    // $(".serviceList ul li").addClass("active");
    $(".serviceList ul li").click(function () {
        $(this).next().slideToggle("slow").removeClass("active");
        $(this).toggleClass("angle");
    });
}


$(".outTeamTab ul li").click(function () {
    var tab_id = $(this).attr("data-tab");
    $(".outTeamTab ul li").removeClass("current");
    $(".tab-content").removeClass("current");
    $(this).addClass("current");
    $("#" + tab_id).addClass("current");
});